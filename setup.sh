#!/bin/bash
function base {
	setupDir=$(pwd)
	basePackages="basePackages.txt"
	baseUser=$(who | awk 'NR==1{print $1}')

	### Perform base system installation ###

	# Setup git info
	git config --global user.name "Simon-Pierre Brazeau"
	git config --global user.email "simon.pierre.brazeau@gmail.com"
  git config --global push.default matching

	# Create directories that don't exist
	codeDir="Code"
	docDir="Documents"
	downDir="Downloads"

	cd

	if [ ! -d "$codeDir" ]; then
		echo Missing code dir...
		sudo -u $baseUser mkdir $codeDir
	fi

	if [ ! -d "$docDir" ]; then
		echo Missing document dir...
		sudo -u $baseUser mkdir $docDir
	fi

	if [ ! -d "$downDir" ]; then
		echo Missing downloads folder...
		sudo -u $baseUser mkdir $downDir
	fi
	
	cd $setupDir

	# Update apt
	apt -y update

	# Install base packages
	pkgString=""
	while IFS= read line
	do
		pkgString="$pkgString $line"
	done <"$basePackages"

	apt -y install $pkgString

	# Get Home config files
	cd
	sudo -u $baseUser git init .
	sudo -u $baseUser git remote add origin https://SimonPierreBrazeau@gitlab.com/SimonPierreBrazeau/my-rice.git
	sudo -u $baseUser git fetch --all
	sudo -u $baseUser git reset --hard origin/master
	cd $setupDir

	# Setup vim
	sudo -u $baseUser git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	sudo -u $baseUser vim +PluginInstall +qall

	# Setup vim youcompleteme
	cd ~/.vim/bundle/youcompleteme
	./install.py --clang-completer
	cd $setupDir

  # Generate sha key
  sudo -u $baseUser ssh-keygen -t rsa -C "simon.pierre.brazeau@gmail.com" -b 4096 -N "" -q
}

function eosSetup {
  setupDir=$(pwd)
  baseUser=$(who | awk 'NR==1{print $1}')
  eosPackages="eosPackages.txt"
  eosJunk="eos-junkPackages.txt"
  eosAptRepos="eos-aptRepos.txt"

  # Add package to add repos
  apt install -y software-properties-common

  # Add repos from file
  while IFS= read line
	do
    add-apt-repository -y $line
	done <"$eosAptRepos"

  # Add chrome repo
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
  echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list

  apt update

  # Install eos packages
  pkgString=""
  while IFS= read line
  do
    pkgString="$pkgString $line"
  done <"$eosPackages"
  
  apt install -y $pkgString

  # Remove junk packages
  pkgString=""
  while IFS= read line
  do
    pkgString="$pkgString $line"
  done <"$eosJunk"
  
  apt purge -y $pkgString

  # Change default terminal to urxvt
  gsettings set org.gnome.desktop.default-applications.terminal exec urxvt
  gsettings set org.gnome.desktop.default-applications.terminal exec-arg ''

  # Update xrdb
  xrdb ~/.Xresources

  # Install discord
  cd ~/Downloads
  wget -O discord.deb https://discordapp.com/api/download?platform=linux&format=deb
  apt install -y ./discord.deb

  # Install steam
  wget -E http://media.steampowered.com/client/installer/steam.deb
  apt install -y ./steam.deb

  rm steam.deb discord.deb

  cd $setupDir

  # Change system fonts
  sudo -u $baseUser gsettings set org.gnome.desktop.interface document-font-name 'Arimo 10'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface font-name 'Arimo 9'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface monospace-font-name 'Cousine 10'
  sudo -u $baseUser gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Arimo Bold 9'

  # Change system theme
  sudo -u $baseUser gsettings set org.gnome.desktop.interface icon-theme 'Numix-Circle'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface gtk-theme 'Arc-Dark'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface gtk-im-module 'gtk-im-context-simple'

  # Change window button layout
  sudo -u $baseUser gsettings set org.pantheon.desktop.gala.appearance button-layout 'close,minimize,maximize'

  # Change keyboard key delay
  sudo -u $baseUser gsettings set org.gnome.desktop.peripherals.keyboard delay 250

  # Add wallpaper
  sudo -u $baseUser cp wallpaper.jpg ~/Pictures/
  sudo -u $baseUser gsettings set org.gnome.desktop.background picture-options 'zoom'
  sudo -u $baseUser gsettings set org.gnome.desktop.background picture-uri 'file:///home/$baseUser/Pictures/wallpaper.jpg'
  
  # Change single click to double click
  sudo -u $baseUser gsettings set org.pantheon.files.preferences single-click false
}

function ubuntuSetup {
  setupDir=$(pwd)
  baseUser=$(who | awk 'NR==1{print $1}')
  ubuntuPackages="ubuntuPackages.txt"
  ubuntuAptRepos="ubuntu-aptRepos.txt"

  # Add repos from file
  while IFS= read line
	do
    add-apt-repository -y $line
	done <"$ubuntuAptRepos"

  # Add chrome repo
  wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
  echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list

  apt update

  # Install eos packages
  pkgString=""
  while IFS= read line
  do
    pkgString="$pkgString $line"
  done <"$ubuntuPackages"
  
  apt install -y $pkgString

  # Change system fonts
  sudo -u $baseUser gsettings set org.gnome.desktop.interface document-font-name 'Arimo 10'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface font-name 'Arimo 9'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface monospace-font-name 'Cousine 10'
  sudo -u $baseUser gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Arimo Bold 9'

  # Change system theme
  sudo -u $baseUser gsettings set org.gnome.desktop.interface icon-theme 'Paper'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface cursor-theme 'Adapta'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface gtk-theme 'Adapta'
  sudo -u $baseUser gsettings set org.gnome.desktop.interface gtk-im-module 'gtk-im-context-simple'

  # Change keyboard key delay
  sudo -u $baseUser gsettings set org.gnome.desktop.peripherals.keyboard delay 250

  # Install pygame
  pip3 install pygame

}

### BEGIN SCRIPT ###
eosReq=false
ubuntuReq=false

while getopts ":e" opt; do
  case $opt in
    u)
      ubuntuReq=true
      ;;
    e)
      eosReq=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG"
      exit 1
      ;;
  esac
done

# Perform base installation
base

if $eosReq
then
  eosSetup
fi

if $ubuntuReq
then
  ubuntuSetup
fi

apt -y update
apt -y dist-upgrade
apt -y autoremove
